// Load the expressjs module into our application and saved it in variable called express
const express = require("express")
const port = 4000;

// app is our server
// create an application that uses express and stores it as an app
const app = express();

// Middleware (request handlers)
// express.json() is a method which allow us to handle the streaming of data and automatically parse the incoming JSON from our req.body
app.use(express.json());

// Mock data
let users = [
    {
        username: "TStark3000",
        email: "starkindustries@mail.com",
        password: "notPeterParker"
    },
    {
        username: "ThorThunder",
        email: "thorStrongestAvenger@mail.com",
        password: "iLoveStormBreaker"
    }
];

app.get("/", (request, response) => {
    {
        response.send("Hello from my first expressJSAPI");
    }
});

// Mini Activity
app.get("/greeting", (request, response) => {
    {
        response.send("Hello from Batch230-Galag");
    }
});

// retrieval of users in mock database
app.get("/users", (req, res) => {
    res.send(users);
})

// POST METHOD
app.post("/users", (request, response) => {

    let newUser = {
        username: request.body.username,
        email: request.body.email,
        password: request.body.password
    }

    users.push(newUser);
    console.log(users);

    response.send(users);
})

// PUT METHOD
app.put("/users/:index", (req, res) => {
    console.log(req.body); // index: 1
    console.log(req.params); // 1
    
    let index = parseInt(req.params.index);
    users[index].password = req.body.password

    res.send(users[index]);
})

// DELETE METHOD
// Delete the last element in an array
app.delete("/users", (req, res) => {

    users.pop()

    res.send(users);
})


let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }
];

// ACTIVITY
/*
    >> Create a new collection in Postman called s34-activity
    >> Save the Postman collection in your s34 folder
*/
// GET
// >> Create a new route to get and send items array in the client (GET ALL ITEMS)
// Insert your code here ...

app.get("/items", (req, res) => {

    res.send(items);
})


// POST
// >> Create a new route to create and add a new itme object in the items array (CREATE ITEM)
    // >> send the updated items array in the client
    // >> check if the POST method route for our users for reference
// Insert your code here ...

app.post("/items", (req, res) => {

    let newItem = {
        name : req.body.name,
        price: req.body.price,
        isActive : req.body.isActive
    }

    items.push(newItem);
    res.send(items);
})


// PUT
// >> Create a new route which can update the price of a single item in the array (UPDATE ITEM)
/*
    >> Pass the index number of the item that you want to update in the request params
    >> add the price update in the request body
    >> reassign the new price from our request body
    >> send the updated item to the client
*/
// Insert your code here ...

app.put("/items/:index", (req, res) => {
    console.log(req.body);
    console.log(req.params)

    let index = parseInt(req.params.index);
    items[index].price = req.body.price

    res.send(items[index]);
})


app.listen(port, () => console.log(`Server is now running at port ${port}`));